# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Meta package for tools needed to build AOSP (and AOSP-based projects) on Gentoo"
HOMEPAGE="https://wiki.gentoo.org/wiki/No_homepage"

LICENSE="metapackage"
SLOT="0"
KEYWORDS="~amd64"

RDEPEND="
	app-arch/lz4
	app-arch/lzop
	app-arch/zip
	app-crypt/gnupg
	dev-build/make
	dev-libs/openssl
	dev-libs/libxml2
	dev-libs/libxslt
	dev-libs/lzo
	dev-util/ccache
	dev-util/gperf
	dev-vcs/git
	llvm-runtimes/libcxx
	media-gfx/imagemagick
	media-gfx/pngcrush
	media-libs/libsdl
	net-misc/curl
	net-misc/rsync
	sys-devel/bc
	sys-devel/bison
	sys-devel/flex
	sys-devel/gcc[multilib,cxx]
	sys-fs/squashfs-tools
	sys-libs/glibc
	sys-libs/ncurses-compat[tinfo,abi_x86_32]
	sys-libs/readline[abi_x86_32]
	sys-libs/zlib[abi_x86_32]
	sys-process/schedtool
	x11-libs/wxGTK
"
