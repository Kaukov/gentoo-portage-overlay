# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

# Taken from Jorgicio's Gentoo overlay and modified

EAPI=8

PYTHON_COMPAT=( python3_{10..13} )

inherit distutils-r1 gnome2 meson

DESCRIPTION="Stop the desktop from becoming idle in full-screen mode. (Fork of Caffeine)"
HOMEPAGE="https://codeberg.org/WhyNotHugo/caffeine-ng"

inherit git-r3

SRC_URI=""
KEYWORDS=""
EGIT_REPO_URI="${HOMEPAGE}.git"
LICENSE="GPL-3"
SLOT="0"
IUSE=""

DEPEND="
	${PYTHON_DEPS}
	dev-python/click[${PYTHON_USEDEP}]
	dev-python/dbus-python[${PYTHON_USEDEP}]
	dev-python/pygobject:3[${PYTHON_USEDEP}]
	dev-python/pulsectl[${PYTHON_USEDEP}]
	dev-python/setuptools[${PYTHON_USEDEP}]
	dev-python/setuptools-scm[${PYTHON_USEDEP}]
	>=dev-python/pyxdg-0.25[${PYTHON_USEDEP}]
	>=dev-python/docopt-0.6.2[${PYTHON_USEDEP}]
	>=dev-python/ewmh-0.1.4[${PYTHON_USEDEP}]
	>=dev-python/setproctitle-1.1.10[${PYTHON_USEDEP}]
	>=dev-python/wheel-0.29.0[${PYTHON_USEDEP}]
	>=dev-libs/libayatana-appindicator-0.5.92
	x11-libs/gtk+:3
	x11-libs/libnotify[introspection]
	>=app-text/scdoc-1.9.7
"
RDEPEND="${DEPEND}
	!x11-misc/caffeine"
